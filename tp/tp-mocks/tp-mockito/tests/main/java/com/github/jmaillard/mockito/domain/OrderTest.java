package main.java.com.github.jmaillard.mockito.domain;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Locale;

import org.junit.Test;
import org.mockito.Mockito;

public class OrderTest {

	@Test
	public void two_product_in_my_order_total_8_dot_99() throws Exception {

		// GIVEN
		Product product1 = Mockito.mock(Product.class);
		Product product2 = Mockito.mock(Product.class);

		Order order = Mockito.spy(new Order(null));

		order.products.add(product1);
		order.products.add(product2);

		// WHEN
		Mockito.when(product1.getPrice()).thenReturn(new BigDecimal("3.99"));
		Mockito.when(product2.getPrice()).thenReturn(new BigDecimal("5"));

		// THEN
		BigDecimal resAttendu = new BigDecimal("8.99");
		assertEquals(resAttendu, order.getTotalPrice());
	}

	@Test
	public void test_format_price() throws Exception {
		// GIVEN
		Product product1 = Mockito.mock(Product.class);
		Product product2 = Mockito.mock(Product.class);

		Order order = Mockito.spy(new Order(null));

		order.products.add(product1);
		order.products.add(product2);

		// WHEN
		Mockito.when(product1.getPrice()).thenReturn(new BigDecimal("3.99"));
		Mockito.when(product2.getPrice()).thenReturn(new BigDecimal("5"));
		
		assertEquals("8,99 �", order.formatTotalPrice(Locale.FRANCE));
	}
}
