#Author: Colin
Feature: tp
	
Background:
 	Given there are 10 places
 	And the cocktail cost 10 euros
 	
 	Scenario Outline: place
 		Given there are <occupy> occupied places
 		When I enter with <enter> people
 		Then the bar <open> 

  Examples:
    | occupy | enter |  open  |
    |   9    |   2   |    0   |
    |   8    |   2   |    1   |
    |   10   |   1   |    0   |
    |   3    |   2   |    1   |
    

 	Scenario Outline: cocktail
 		Given they tooks <nbCocktail> cocktails
 		And they have already taken <number> cocktails
 	  And <person> pay the totality
 	  Then Mr Pignon is <happy because he only had one drink>
 	  
 	Examples:
    |   nbCocktail   |number |    person        | happy because he only had one drink  | 
    |  2             | 0		 |  "Mr le blanc"   |   1                                  | 
    |  2             | 0		 |  "Mr Pignon"     |   1                                  | 
    |  2             | 2		 |  "Mr le blanc"   |   0                                  | 
    
   